import pandas as pd
import numpy as np
from pprint import pprint

mapping = {
    'unit_number': 'unit',
    'street_number': 'houseNumber',
    'locality_name': 'city',
    'region_code': 'stateAbbreviation',
    'postcode': 'postcode',
    'street_name': 'street'
}

csv_index = [
    'unit_number',
    'street_number',
    'locality_name',
    'region_code',
    'postcode',
    'street_name'
]
# csv_index = [0,1,2,3,4,5]

df = pd.read_csv(filepath_or_buffer='AGL_WA.csv', dtype=str)
df.dropna(subset=['question_name'], inplace=True)
df.replace(np.nan, '', regex=True, inplace=True)
# df['new_index'] = df['unit'].map(str) + df['houseNumber'] + df['city'] + df['stateAbbreviation'] + df['postcode'] + df['street'] + df['question_name']
# df.set_index('new_index', inplace=True)
# df = df[~df.index.duplicated(keep='last')]
pprint(df.columns)
df = df.pivot_table(columns='question_name', values='answer', index=csv_index, aggfunc='last')
df.reset_index(inplace=True)
# df = df.melt(id_vars=csv_index).dropna()


# df = df.groupby(['unit', 'houseNumber', 'city', 'stateAbbreviation', 'postcode', 'street'])['answer'].apply(lambda x: x.sum())

df.rename(columns=mapping, inplace=True)
result_obj = df.select_dtypes(['object'])
df[result_obj.columns] = result_obj.apply(lambda x: x.str.strip())
df = df.T
pprint(df.info())
df.to_csv('result_agl_iknock.csv', sep=';')
