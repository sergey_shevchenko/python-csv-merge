from sklearn import datasets
from sklearn import svm
from pprint import pprint

digits = datasets.load_digits()
clf = svm.SVC(gamma=0.001, C=100.)
clf.fit(digits.data[:-1], digits.target[:-1])
print("prediction:\t{}".format(clf.predict(digits.data[-1:])[0]))
print("real data:\t{}".format(digits.target[-1:][0]))
