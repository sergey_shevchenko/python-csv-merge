import pandas as pd
import os
import numpy as np
from pprint import pprint


# function that splits one big csv to multiple smaller csvs
def split(filehandler, delimiter=';', row_limit=500000,
          output_name_template='output_%s.csv', output_path='.', keep_headers=True):
    import csv

    reader = csv.reader(filehandler, delimiter=delimiter)
    current_piece = 1
    current_out_path = os.path.join(
        output_path,
        output_name_template % current_piece
    )
    current_out_writer = csv.writer(open(current_out_path, 'w'), delimiter=delimiter)
    current_limit = row_limit
    if keep_headers:
        headers = next(reader)
        current_out_writer.writerow(headers)
    for i, row in enumerate(reader):
        if i + 1 > current_limit:
            current_piece += 1
            current_limit = row_limit * current_piece
            current_out_path = os.path.join(
                output_path,
                output_name_template % current_piece
            )
            current_out_writer = csv.writer(open(current_out_path, 'w'), delimiter=delimiter)
            if keep_headers:
                current_out_writer.writerow(headers)
        current_out_writer.writerow(row)


headers = {
    'kvk8': "kvk",
    'vestigingsnummer': "vestigingsnummer",
    'zaaknaam': "name",
    'straat': "street",
    'huisnummer': "houseNumber",
    'toevoeginghuisnummer': "houseSuffix",
    'postcode': "postcode",
    'woonplaatsnaam': "city",
    'edsn_ean_elektra': "ean_code_elektra",
    'edsn_ean_gas': "ean_code_gas",
    'edsn_kleinverbruik_elektra': "dl_aansluitingelecklein",
    'edsn_kleinverbruik_gas': "dl_aansluitinggasklein",
    'edsn_grootverbruik_elektra': "dl_aansluitingelecgroot",
    'edsn_grootverbruiker_elektra': "dl_aansluitinggasgroot",
    'zzp': "dl_zzp",
    'dnb_telefoonnummer': "telefoonnummer",
    'bevoegd_functionaris_geslacht': "bevoegd sourcefunctionaris geslacht",
    'bevoegd_functionaris_initiaal1': "bevoegd functionaris voornaam",
    'bevoegd_functionaris_initiaal2': "bevoegd functionaris tussenvoegsel",
    'bevoegd_functionaris_initiaal3': "bevoegd functionaris tussenvoegsel 2",
    'bevoegd_functionaris_achternaam': "bevoegd functionaris achternaam",
    'domeinnaam': "website",
    'aantal_medewerkers': "aantal medewerkers",
    'aantal_medewerkers_concern': "aantal bedrijven in concern",
    'bag_oppervlakte': "gebouw oppervlakte",
    'bag_bouwjaar1': "gebouw bouwjaar 1",
    'sbi_code_primair': "sbi-code",
    'sbi_omschrijving_primair': "sbi-omschrijving",
    'land': "country",
    'EXPECTED_ENERGY_USAGE': "Verbruik_E",
    'EXPECTED_GAS_USAGE': "Verbruik_G",
    'is_customer': 'dl_bestaandeklant',
    'kvknummer_vestigingsnummer': 'source_KvK_vestigingsnummer'

}

olbico = pd.read_csv(filepath_or_buffer='export_smart_kvk.csv', sep='|', index_col=2, encoding='latin-1', dtype=str)
# olbico = pd.read_csv(filepath_or_buffer='export_smart_kvk.csv', sep='|', encoding='latin-1', dtype=str)
dikw = pd.read_csv(filepath_or_buffer='eneco-classes-2018-09-14-16-17-23.csv', sep=',', index_col=0, encoding='latin-1', dtype=str)
# dikw = pd.read_csv(filepath_or_buffer='eneco-classes-2018-09-14-16-17-23.csv', sep=',', encoding='latin-1', dtype=str)
# clients = pd.read_csv(filepath_or_buffer='Klantendump.csv', sep=';', dtype=str)
# clients.rename(index=str, columns={
#    'LEVERING_POSTCODE': 'postcode',
#    'LEVERING_HUISNR': 'huisnummer',
#    'LEVERING_HUISNR_TOEV': 'toevoeginghuisnummer'
# }, inplace=True)
frames = [olbico, dikw]

# megre
result = pd.concat(objs=frames, axis=1, sort=True)
# result = olbico.merge(dikw, left_on='kvknummer_vestigingsnummer', right_on='KVK8_VESTINGSNUMMER', how='left')

# result = result.merge(clients, on=['postcode', 'huisnummer', 'toevoeginghuisnummer'], how='left')
# result['is_customer'].replace(np.nan, '0', inplace=True)
# result = result.join(clients, on=('postcode', 'huisnummer', 'toevoeginghuisnummer'))

# rename column headers
result.rename(index=str, columns=headers, inplace=True)

# columns we don't need
# result.drop(columns=['handelsnaam', 'vennootschapsnaam'], inplace=True)
# result.drop(columns=['KVK8_VESTINGSNUMMER'], inplace=True)

# remove empty strings
# result['postcode'].replace('', np.nan, inplace=True)
result.dropna(subset=['postcode'], inplace=True)
result['Verbruik_E'].replace('', np.nan, inplace=True)
result.dropna(subset=['Verbruik_E'], inplace=True)

# add static columns
newColumnLoc = len(list(result.columns.values))
# result.insert(column="agent", value="", loc=newColumnLoc)
# newColumnLoc += 1
# result.insert(column="assign_to_team", value="", loc=newColumnLoc)
# newColumnLoc += 1
# result.insert(column="bevoegd functionaris functieomschrijving", value="", loc=newColumnLoc)  # not found
# newColumnLoc += 1
# result.insert(column="afstand tot laadstation (meter)", value="", loc=newColumnLoc)  # not found
# newColumnLoc += 1
# result.insert(column="dl_niemandAanwezig", value="0", loc=newColumnLoc)
# newColumnLoc += 1
# result.insert(column="dl_contractgetekend", value="0", loc=newColumnLoc)
# newColumnLoc += 1
# result.insert(column="dl_geenInteresse", value="0", loc=newColumnLoc)
# newColumnLoc += 1
# result.insert(column="dl_NiemandaanwezigII", value="0", loc=newColumnLoc)
# newColumnLoc += 1
# result.insert(column="dl_contractverlenging", value="0", loc=newColumnLoc)

# newColumnLoc += 1
result.insert(column="dl_verbruikeleclaag", value="0", loc=newColumnLoc)
result['dl_verbruikeleclaag'] = ['1' if x == 'laag' else '0' for x in result['Verbruik_E']]
newColumnLoc += 1
result.insert(column="dl_verbruikgaslaag", value="0", loc=newColumnLoc)
result['dl_verbruikgaslaag'] = ['1' if x == 'laag' else '0' for x in result['Verbruik_G']]
newColumnLoc += 1
result.insert(column="dl_verbruikelecmidden", value="0", loc=newColumnLoc)
result['dl_verbruikelecmidden'] = ['1' if x == 'midden' else '0' for x in result['Verbruik_E']]
newColumnLoc += 1
result.insert(column="dl_verbruikgasmidden", value="0", loc=newColumnLoc)
result['dl_verbruikgasmidden'] = ['1' if x == 'midden' else '0' for x in result['Verbruik_G']]
newColumnLoc += 1
result.insert(column="dl_verbruikelechoog", value="0", loc=newColumnLoc)
result['dl_verbruikelechoog'] = ['1' if x == 'hoog' else '0' for x in result['Verbruik_E']]
newColumnLoc += 1
result.insert(column="dl_verbruikgashoog", value="0", loc=newColumnLoc)
result['dl_verbruikgashoog'] = ['1' if x == 'hoog' else '0' for x in result['Verbruik_G']]

# change boolean columns to 0 or 1
result['dl_aansluitingelecklein'] = result['dl_aansluitingelecklein'].map(
    {'True': "1", 'False': "0", 'Onbekend': "0", '': "0"}).fillna(0).astype(str)
result['dl_aansluitinggasklein'] = result['dl_aansluitinggasklein'].map(
    {'True': "1", 'False': "0", 'Onbekend': "0", '': "0"}).fillna(0).astype(str)
result['dl_aansluitingelecgroot'] = result['dl_aansluitingelecgroot'].map(
    {'True': "1", 'False': "0", 'Onbekend': "0", '': "0"}).fillna(0).astype(str)
result['dl_aansluitinggasgroot'] = result['dl_aansluitinggasgroot'].map(
    {'True': "1", 'False': "0", 'Onbekend': "0", '': "0"}).fillna(0).astype(str)

newColumnLoc += 1
result.insert(column="aansluitwaarde electra", value="", loc=newColumnLoc)
newColumnLoc += 1
result.insert(column="aansluitwaarde gas", value="", loc=newColumnLoc)
result.ix[result.dl_aansluitingelecgroot == "1", 'aansluitwaarde electra'] = 'groot'
result.ix[result.dl_aansluitinggasgroot == "1", 'aansluitwaarde gas'] = 'groot'
result.ix[result.dl_aansluitingelecklein == "1", 'aansluitwaarde electra'] = 'klein'
result.ix[result.dl_aansluitinggasklein == "1", 'aansluitwaarde gas'] = 'klein'


# result['dl_zzp'] = result['dl_zzp'].map({'Ja': "1", 'Nee': "0", 'Onbekend': "0", '': "0"}).fillna(0).astype(str)
# result['dl_bestaandeklant'] = result['dl_bestaandeklant'].map({'Ja': "1", 'Nee': "0", 'Onbekend': "0", '': "0"}).fillna(0).astype(str)

# result['dl_bestaandeklant'] = result['dl_bestaandeklant'].str
# result['dl_bestaandeklant'] = result['dl_bestaandeklant'].fillna(0)


result.drop(columns=[
    "kvk",
    "vestigingsnummer",
    "name",
    # "street",
    # "houseNumber",
    # "houseSuffix",
    # "postcode",
    # "city",
    "ean_code_elektra",
    "ean_code_gas",
    "dl_zzp",
    "telefoonnummer",
    "bevoegd sourcefunctionaris geslacht",
    "bevoegd functionaris voornaam",
    "bevoegd functionaris tussenvoegsel",
    "bevoegd functionaris tussenvoegsel 2",
    "bevoegd functionaris achternaam",
    "website",
    "aantal medewerkers",
    "aantal bedrijven in concern",
    "gebouw oppervlakte",
    "handelsnaam",
    "vennootschapsnaam",
    "gebouw bouwjaar 1",
    "sbi-code",
    "sbi-omschrijving",
    # "country"
], inplace=True)

# set index to be source_KvK_vestigingnummer column
result.reset_index(level=0, inplace=True)
result.rename(index=str, columns={'index': 'source_KvK_vestigingsnummer'}, inplace=True)
pprint(result.info())

# trim the strings
result_obj = result.select_dtypes(['object'])
result[result_obj.columns] = result_obj.apply(lambda x: x.str.strip())
# pprint(result.groupby(result.columns.tolist(), as_index=False).size())
# pprint(result.groupby(['source_KvK_vestigingsnummer'], as_index=False).size().reset_index().rename(columns={0:'Count'}).sort_values('Count', ascending=False))

# write to one big file just in case
result.to_csv('result.csv', sep=';')

# split file to several smaller
split(open('result.csv', 'r'))
